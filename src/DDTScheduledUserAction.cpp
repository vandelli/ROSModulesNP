//$Id: DDTScheduledUserAction.cpp 56588 2009-02-04 09:44:31Z joos $
/************************************************************************/
/*									*/
/* File             : DDTScheduledUserAction.cpp			*/
/*									*/
/* Authors          : M. Joos, J.Petersen 				*/
/*									*/
/********* C 2009 - ROS/RCD *********************************************/

#include <iomanip>
#include <sys/types.h>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSRobinNP/RolNP.h"
#include "ROSModulesNP/ModulesException.h"
#include "ROSModulesNP/DDTScheduledUserAction.h"

using namespace ROS;


/******************************************************************************************************/
DDTScheduledUserAction::DDTScheduledUserAction(RobinNP *robIn, u_int rolPhysicalAddress, int deltaTimeMs,
		u_int triggerQueueSize, u_int /*triggerQueueUnblockOffset*/)
: ScheduledUserAction(deltaTimeMs)
/******************************************************************************************************/
{ 
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::constructor: Entered");

	m_rol = &(robIn->getRol(rolPhysicalAddress));

	// with ECR jumps, L1ID = 0xffffffff is an illegal/impossible L1ID
	m_mostRecentIdSent     = 0xffffffff;
	m_mostRecentIdReceived = 0xffffffff;

  m_reactToEnabled       = false;
  m_inputToTriggerQueue  = TriggerInputQueue::instance();
  m_inputToTriggerQueue->setSize(triggerQueueSize);
  m_deltaTimeMs          = deltaTimeMs;
  m_low                  = deltaTimeMs;

	m_hist = new int[m_bins];
	for (int i = 0; i < m_bins; i++)
		m_hist[i] = 0;

	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::constructor: Done");
}


/***********************************************/
DDTScheduledUserAction::~DDTScheduledUserAction() 
/***********************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::destructor: Entered");

  m_inputToTriggerQueue->destroy();
  //std::cout << " ~DDTScheduledUserAction: destroy Q, status = " << ret << std::endl;

	delete [] m_hist;
}

inline void DDTScheduledUserAction::pushToQueue(u_int firstToPush, u_int lastToPush) {
   if (m_reactToEnabled){
      DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: push first " << HEX(firstToPush) << " onto Q");
      m_inputToTriggerQueue->push(firstToPush);
   }
   if (m_reactToEnabled){
      DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: push last " << HEX(lastToPush) << " onto Q");
      m_inputToTriggerQueue->push(lastToPush);
   }
}

//Called by UserActionScheduler::run
/****************************************/
void DDTScheduledUserAction::reactTo(void)
/****************************************/
{
	ECRStatisticsBlock ecr_info;

	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo entered");

	if (m_reactToEnabled == false)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: m_reactToEnabled = false. Return immediately");
		return;
	}

	DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo. Last time slice = "  << getLastTimeSlice() << " ms");

	// get the ECR information
	ecr_info = m_rol->getECR();
	DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: Most recent Id  = 0x" << HEX(ecr_info.mostRecentId));
	DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: Overflow        = " << ecr_info.overflow);
	DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: Number of ECRs  = " << ecr_info.necrs);
	for (u_int loop = 0; loop < ecr_info.necrs; loop++)
		DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: Last L1ID before ECR # " << loop + 1 << " = 0x" << HEX(ecr_info.ecr[loop]));

	if (ecr_info.overflow != 0)
	{
		CREATE_ROS_EXCEPTION(e, ModulesException, DDT_ECROVERFLOW, " # ECRs = " << ecr_info.necrs);
		throw(e);
	}

	m_mostRecentIdReceived = ecr_info.mostRecentId;

	if (m_mostRecentIdSent == 0xffffffff)       // no fragments processed, yet
	{
		if (m_mostRecentIdReceived != 0xffffffff) // fragments arrived
		{
			if (ecr_info.necrs > 0)                 // new ECRs: handle complete bunches; rather unlikely in practice ...
			{
				DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: first probe with fragments: complete bunches");
				for (u_int l_ecr = 0; l_ecr < ecr_info.necrs; l_ecr++)
				{
					DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: first probe with fragments: ECR # = " << l_ecr);

					u_int firstToPush = ecr_info.ecr[l_ecr] & 0xff000000;	// first of previous bunch
					u_int lastToPush = ecr_info.ecr[l_ecr];	        // last of previous bunch

                                        pushToQueue(firstToPush,lastToPush);
                                }
			}

			DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: first probe with fragments: last, possibly only and incomplete ECR bunch");

			u_int firstToPush = m_mostRecentIdReceived & 0xff000000;	// first of current bunch
			u_int lastToPush  = m_mostRecentIdReceived;		// last of current bunch
                        pushToQueue(firstToPush,lastToPush);
			m_mostRecentIdSent = lastToPush;
		}
		else // nothing yet
		{
			DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: first probes: no fragments yet");
		}
	}	// first time
	else  // not first time we probe successfully
	{	
		DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: mostRecentIdReceived  = " << HEX(m_mostRecentIdReceived));
		DEBUG_TEXT(DFDB_ROSFM, 20, "DDTScheduledUserAction::reactTo: mostRecentIdSent      = " << HEX(m_mostRecentIdSent));

		if (m_mostRecentIdReceived == m_mostRecentIdSent) // no new fragments
		{
			DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: fragments stopped flowing");
		}
		else
		{
			if (ecr_info.necrs == 0) // no new ECRs
			{
				if (m_mostRecentIdReceived > m_mostRecentIdSent) // more fragments in current bunch
				{
					DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: more fragments in current bunch ");
					u_int firstToPush = m_mostRecentIdSent + 1;
					u_int lastToPush  = m_mostRecentIdReceived;
                                        pushToQueue(firstToPush,lastToPush);
					m_mostRecentIdSent = lastToPush;
				}
				else // L1ID wrap without ECR: hack to handle RODs which wrap EXACTLY at 24 bits
				{
					std::cout << "DDTScheduledUserAction::reactTo: L1ID wrap with no ECR, 24 bit ROD" << std::endl;
					std::cout << "DDTScheduledUserAction::reactTo: mostRecentIdReceived  = " << HEX(m_mostRecentIdReceived) << std::endl;
					std::cout << "DDTScheduledUserAction::reactTo: mostRecentIdSent      = " << HEX(m_mostRecentIdSent) << std::endl;

					u_int firstToPush = m_mostRecentIdSent + 1;	// possibly = lastToPush
					u_int lastToPush  = 0xffffff;			// 24 bit

					if (m_mostRecentIdSent < 0xffffff)
					{
                                           pushToQueue(firstToPush,lastToPush);
					}

					firstToPush = 0;
					lastToPush = m_mostRecentIdReceived;
                                        pushToQueue(firstToPush,lastToPush);
					m_mostRecentIdSent = lastToPush;
				}
			}

			if (ecr_info.necrs > 0) // new ECRs
			{
				if (ecr_info.ecr[0] > m_mostRecentIdSent) // remainder of previous bunch
				{
					DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: remainder of previous bunch ");
					u_int firstToPush = m_mostRecentIdSent + 1;
					u_int lastToPush = ecr_info.ecr[0];
                                        pushToQueue(firstToPush,lastToPush);
                                }
				else
				{
					DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: previous bunch was completed");
				}

				if (ecr_info.necrs > 1) // handle intermediate complete ECR bunches: rather unlikely case
				{
					for (u_int l_ecr = 1; l_ecr < ecr_info.necrs; l_ecr++)
					{
						DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: intermediate bunches");

						u_int firstToPush = ecr_info.ecr[l_ecr] & 0xff000000;	// first of previous bunch
						u_int lastToPush  = ecr_info.ecr[l_ecr];			// last of previous bunch
                                                pushToQueue(firstToPush,lastToPush);
					}
				}

				DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo: last ECR bunch");

				u_int firstToPush = m_mostRecentIdReceived & 0xff000000;	// first of current bunch
				u_int lastToPush  = m_mostRecentIdReceived;			// last of current bunch
                                pushToQueue(firstToPush,lastToPush);
				m_mostRecentIdSent = lastToPush;
			}		// new ECRs
		}		// new fragments
	}		// not first time

	// statistics on action scheduling
	int lastTimeSlice = getLastTimeSlice();

	int low = m_low;
	for (int i = 1; i < (m_bins - 1); i++)
	{
		if ((low <= lastTimeSlice) && (lastTimeSlice < (low + m_width)))
			m_hist[i]++;

		low += m_width;
	}

	if (lastTimeSlice > low) //overflow
		m_hist[m_bins - 1]++;

	if (lastTimeSlice < m_low) //underflow
		m_hist[0]++;

	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::reactTo done");
}


//Called by IOManager::prepareForRun
/**********************************************/
void DDTScheduledUserAction::prepareForRun(void)
/**********************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::prepareForRun entered");
	std::cout << "DDTScheduledUserAction::prepareForRun entered" << std::endl;

	m_mostRecentIdSent     = 0xffffffff;
	m_mostRecentIdReceived = 0xffffffff;
	m_reactToEnabled       = true;
	m_inputToTriggerQueue->reset();
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::prepareForRun done");
}


//Called by IOManager::stopEB
/***************************************/
void DDTScheduledUserAction::stopEB(void)
/***************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::stopEB entered");
	std::cout << "DDTScheduledUserAction::stopEB entered" << std::endl;
	m_reactToEnabled = false;
        m_inputToTriggerQueue->abort();
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::stopEB done");
}


//Called by RobinReadoutModule::publishFullStatistics
/***************************************************/
void DDTScheduledUserAction::printTimeHistogram(void)
/***************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::printTimeHistogram entered");

	std::cout << " Scheduler Time Distribution, DeltaTimeMs = " << m_deltaTimeMs << std::endl;
	for (int i = 0; i < (m_bins - 1); i++)
		std::cout << std::setw(10) << (m_low + i * m_width);

	std::cout << std::endl;

	for (int i = 1; i < m_bins; i++) // no underflow
		std::cout << std::setw(10) << m_hist[i];

	std::cout << std::endl;

	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::printTimeHistogram done");
}


//Called by RobinReadoutModule::clearInfo
/******************************************/
void DDTScheduledUserAction::clearInfo(void)
/******************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::clearInfo entered");
	for (int i = 0; i < m_bins; i++)
		m_hist[i] = 0;
	DEBUG_TEXT(DFDB_ROSFM, 15, "DDTScheduledUserAction::clearInfo done");
}
