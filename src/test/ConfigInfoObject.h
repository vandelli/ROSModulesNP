#ifndef CONFIGINFOOBJECT_H
#define CONFIGINFOOBJECT_H

#include <sys/types.h>

#include "DFSubSystemItem/Config.h"

using namespace ROS;

class ConfigInfoObject
{
public:
	ConfigInfoObject(void);
	int SetParams(DFCountedPointer<Config> configuration);
	void SetLinkMask(int mask);
	void SetRemoteStartCmd(char *cmd);
	void SetRemoteComputer(char *name);
	void SetRemoteKillCmd(char *cmd);
	void SendRemoteCommand(void);
	void SendRemoteKill(void);

	char dolarScriptName[80], dolarComputerName[80], nameOfConfigFile[80], nameOfKillScript[80];
	char remoteStartCmd[200], remoteKillCmd[200];
	bool externalDataGeneratorFlag, oneCycleFlag, nonIntPerformMeasFlag, verboseFlag;
	float setupNeventFraction, initialStepReducFactor, initialValueReducFactor, requestFraction;
	int deleteGroupSize, numberOfLVL1AcceptsPerCycle, linkMask;
	u_int maxRXPages, memoryPoolPageSize, memoryPoolNumPages;
	int pageSize, numPages, numberOfWordsPerFragment, pciSlotNumber;
	u_int firmwareVersion, check_verbosity;
	bool dataCheckFlag, dataDumpFlag, iterateFlag, clearAllFlag, noClearsFlag;
	float targetDeleteRate;
	static const unsigned int maxChannels = 12;
	bool debugPrintFlag;
};

#endif //CONFIGINFOOBJECT_H
