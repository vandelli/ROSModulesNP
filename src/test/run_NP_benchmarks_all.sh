#!/bin/bash

################################################################################################################################################################
#
#Robin NP Benchmarking script to run multiple test_Robin jobs with varying word sizes collecting statistics based on a specified number of runs per word size
#
#usage: source run_NP_benchmarks_all.sh <min no. of words> <max no. of words> <step size> <number of runs per size> <readout percentage>
#
#e.g: to run between 50 and 250 words (inclusive), in steps of 50 words and collecting stats from 10 runs per size:
#
# ./run_NP_benchmarks_all.sh 50 250 50 10 100
#
# This will run 10 jobs each at 50, 100, 150, 200 and 250 words and generate stats accordingly with 100% readout
#
# Currently Collected Stats:
#
# 1) Mean and Standard Error for L1 Rate
# 2) Mean and Standard Error for Bandwidth
# 3) Mean and Standard Error for Time Per Event
#
################################################################################################################################################################

let numjobs=$2-$1 #does this need to be on a seperate line?
let numjobs=($numjobs/$3)+1 #number of data points to collect, in increments of 50 words
iterations=$4; #number of iterations to run per data point
readout=$5;
echo "Running $4 jobs at $numjobs data points between $1 and $2 words (inclusive) in steps of $3 words with $5 percent readout"

machine=`uname -n | awk -F. '{print $1}'`
echo "Testing on test machine $machine"

if [ ! -d results_$machine ]; then
    echo "Creating results directory"
    mkdir results_$machine #make results directory if non-existent
fi

#Clear previous runs - remove 'alldata' file seperately as could have been created by a single run not using the script, hence the summary and log files may not exist
if [ -f results_alldata.csv ]
then
    echo "Clearing previous runs"
    rm results_alldata.csv #created by test_Robin
fi

if [ -f results_summary.csv ]
then
    echo "Clearing previous logs and summaries"
    rm results_summary.csv
    rm benchmarklog.log
fi


echo 'Number of Words,Mean L1 Rate,Std Err L1 Rate,Mean Request Rate,Std Err Request Rate,Mean Bandwidth,Std Err Bandwidth,Mean Time Per Event,Std Err Time Per Event' > results_summary.csv

size=$1

jobno=1
while [ $jobno -le $numjobs ];
  do
  runnumber=1
  while [ $runnumber -le $iterations ];
    do
    test_RobinNP -p -r ${readout} -w ${size} -P 0 -m 4095 -v > benchmarklog.log
    let runnumber=runnumber+1;
  done
  
 #Invoke AWK to calculate statistics  
  awk -F',' -v x=$size '{if($4==x) {sumL1+=$1; sumRR+=$2; sumBW+=$6; sumT+=$7; total++; sumL1sq+=$1*$1; sumRRsq+=$2*$2; sumBWsq+=$6*$6; sumTsq+=$7*$7}} END {print x "," sumL1/total "," (sqrt((sumL1sq/total)-(sumL1/total)**2)/sqrt(total)) "," sumRR/total "," (sqrt((sumRRsq/total)-(sumRR/total)**2)/sqrt(total)) "," sumBW/total ","  (sqrt((sumBWsq/total)-(sumBW/total)**2)/sqrt(total)) "," sumT/total "," (sqrt((sumTsq/total)-(sumT/total)**2)/sqrt(total)) }' results_alldata.csv >> results_summary.csv
  
  let size=($jobno*50)+$1;
  let jobno=jobno+1;

done

#Backup all results
echo "Backing up results"
datetime=`date +%d%m%Y_%H%M_%S`
echo $datetime
mkdir "results_$machine/benchmark_testRobin_$datetime"
cp results_alldata.csv "results_$machine/benchmark_testRobin_$datetime"
cp results_summary.csv "results_$machine/benchmark_testRobin_$datetime"
cp benchmarklog.log "results_$machine/benchmark_testRobin_$datetime"
