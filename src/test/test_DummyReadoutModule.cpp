/****************************************************************/
/*								*/
/*  file test_DummyReadoutModule.cpp				*/
/*								*/
/*  exerciser for the DummyReadoutModule class			*/
/*								*/
/*  The program allows to:					*/
/*  . exercise the methods of the DummyReadoutModule Class	*/
/*  . measure the performance					*/
/*								*/
/***C 2004 - A nickel program worth a dime***********************/

#include <iostream.h>
#include <iomanip.h>
#include <vector>
#include <ipc/partition.h>
#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include "ROSGetInput/get_input.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSModulesNP/DummyReadoutModule.h"
#include "ROSModulesNP/PCMemoryDataChannel.h"
#include "DFSubSystemItem/Config.h"
#include "DFDebug/DFDebug.h"

using namespace ROS;

enum 
{ 
  CREATE = 1,
  SETUP,
  CONFIGURE,
  CONNECT,
  START,
  PAUSE,
  RESUME,
  STOP,
  DISCONNECT,
  UNCONFIGURE,
  DELETE,
  DUMP_CHANNELS,
  REQUEST,
  GET,
  RELEASEMANY,
  GETINFO,
  HELP,
  DEBUG,
  QUIT = 100
};

#define FM_POOL_NUMB     10      //Max. number of fragment managers

/************/
int main(void)
/************/ 
{
int num, value, fl1id, l1id, option, quit_flag = 0;
ReadoutModule* fm[FM_POOL_NUMB] = {0};
ROBFragment* robfragment;
vector <unsigned int> idgroup;
int nChannels = 0;
vector<DataChannel *>* vChannels = 0; 
DataChannel *channel = 0;
DFCountedPointer<Config> info;

DFCountedPointer<Config> configuration = Config::New();
try 
{
  IPCCore::init( argc, argv );
}
catch( daq::ipc::Exception & ex ) 
{
  ers::fatal( ex );
}

do 
  {
  std::cout << std::endl << std::endl;
  std::cout << " Create a DummyReadout Module               : " << dec << CREATE << std::endl; 
  std::cout << " Call `setup`         on DummyReadout Module: " << dec << SETUP << std::endl;
  std::cout << " Call `configure`     on DummyReadout Module: " << dec << CONFIGURE << std::endl;
  std::cout << " Call `connect`       on DummyReadout Module: " << dec << CONNECT << std::endl;
  std::cout << " Call `prepareForRun` on DummyReadout Module: " << dec << START << std::endl;
  std::cout << " Call `pause`         on DummyReadout Module: " << dec << PAUSE << std::endl;
  std::cout << " Call `resume`        on DummyReadout Module: " << dec << RESUME << std::endl;
  std::cout << " Call `stopEB`        on DummyReadout Module: " << dec << STOP << std::endl;
  std::cout << " Call `unconfigure`   on DummyReadout Module: " << dec << UNCONFIGURE << std::endl; 
  std::cout << " Call `disconnect`       on DummyReadout Module: " << dec << DISCONNECT << std::endl;
  std::cout << " Delete a Readout Module                    : " << dec << DELETE << std::endl; 
  std::cout << " Dump channel IDs                           : " << dec << DUMP_CHANNELS << std::endl; 
  std::cout << " Request a ROB fragment                     : " << dec << REQUEST << std::endl; 
  std::cout << " Get a ROB fragment                         : " << dec << GET << std::endl; 
  std::cout << " Release many ROB fragments                 : " << dec << RELEASEMANY << std::endl; 
  std::cout << " Call `getInfo`       on DummyReadout Module: " << dec << GETINFO << std::endl;
  std::cout << " HELP                                       : " << dec << HELP << std::endl;
  std::cout << " Set debug parameters                       : " << dec << DEBUG << std::endl;
  std::cout << " QUIT                                       : " << dec << QUIT << std::endl;
  std::cout << " option> ";
  option = getdec();    
  switch(option)   
  {
    case CREATE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] != 0)
      {
        std::cout << "This array element has already been filled"  << std::endl;
        break;
      }
      
      fm[num] = new DummyReadoutModule();
      std::cout << "Readout Module created" << std::endl;
      break;
      
    case SETUP:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     

      std::cout << "Enter the value for the number of Data Channels" << std::endl;
      nChannels = getdecd(1);
      configuration->set("numberOfChannels", nChannels);

      for (int i=0; i<nChannels; i++) 
      {	
	std::cout << "Enter the ID of channel " << i << std::endl;
	value = getdecd(i);
	ostringstream str1;
	str1 << "channel" << i << "Id";
	configuration->set(str1.str(), value);

	std::cout << "Enter the physical address of channel " << i<< std::endl;
	value = getdecd(i);
	ostringstream str2;
	str2 << "channel" << i << "PhysicalAddress";
	configuration->set(str2.str(), value);

	ostringstream str3;
	str3 << "memoryPoolNP" << i << "NumPages";
	std::cout << "Enter the value for <" << str3.str() << ">" << std::endl;
	value = getdecd(100);
	configuration->set(str3.str(), value);
	
	ostringstream str4;
	str4 << "memoryPoolNP" << i << "PageSize";
	std::cout << "Enter the value for <" << str4.str() << ">" << std::endl;
	value = getdecd(1024);
	configuration->set(str4.str(), value);     
	
	ostringstream str5;
	str5 << "pool" << i << "Type";
	std::cout << "Enter the value for <" << str5.str() << "> (1=malloc 2=CMEM)" << std::endl;
	value = getdecd(1);
	configuration->set(str5.str(), value);     
      }

      configuration->set("triggerQueue", 0);
       
      try 
      {
        fm[num]->setup(configuration);
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }
      break;
      
        
    case CONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }  
      
      try 
      {
        fm[num]->configure();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }        
      break; 
      
    case CONNECT:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }  
      try 
      {
        fm[num]->connect();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }        
      break; 
      
    case START:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }  
      try 
      {
        fm[num]->prepareForRun();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }        
      break;  
     
    case PAUSE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }  
      try 
      {
        fm[num]->pause();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }        
      break;  
      
    case RESUME:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }  
      try 
      {
        fm[num]->resume();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }        
      break;  
      
    case STOP:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     
      try 
      {
        fm[num]->stopEB();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }     
      break; 

    case DISCONNECT:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     

      fm[num]->disconnect();
      break;
      
    case UNCONFIGURE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     
      try 
      {
        fm[num]->unconfigure();
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
	break;
      }     
      break;      
      
    case DELETE:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
       
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }     
      delete fm[num];
      break;

    case DUMP_CHANNELS:
      nChannels = DataChannel::numberOfChannels();
      vChannels = DataChannel::channels();
      for (int i = 0; i < nChannels; i++) 
	std::cout << i << ": Channel ID " << (*vChannels)[i]->id() << " Physical Address " << (*vChannels)[i]->physicalAddress()<< std::endl;	  
      break;

    case REQUEST:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try 
      {
	channel = DataChannel::channel(num);
      }
      catch (ModulesException &e) 
      {
	std::cout << e << std::endl;
	break;
      }
      
      std::cout << "Enter the Level 1 ID" << std::endl;
      l1id = getdecd(1); 
         
      value = channel->requestFragment(l1id); 
      std::cout << "Request returns the ticket " << value << std::endl;   
      break;
      
    case GET:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try 
      {
	channel = DataChannel::channel(num);
      }
      catch (ModulesException &e) 
      {
	std::cout << e << std::endl;
	break;
      }
      
      std::cout << "Enter the ticket received from request()" << std::endl;
      l1id = getdecd(1); 
         
      try 
      {
        robfragment = dynamic_cast<ROS::ROBFragment*>(channel->getFragment(l1id)); 
      }
      catch (ModulesException& e) 
      {
        std::cout << e << std::endl;
        break;
      }
      std::cout << "Dumping ROB fragment" << std::endl;
      std::cout << *robfragment;
      delete robfragment;
      break;
      
    case RELEASEMANY:
      std::cout << "Enter the channel id " << std::endl;
      num = getdec();
       
      try 
      {
	channel = DataChannel::channel(num);
      }
      catch (ModulesException &e) 
      {
	std::cout << e << std::endl;
      }
      
      std::cout << "Enter the first L1ID to be released" << std::endl;
      fl1id = getdecd(1);      
      
      std::cout << "How many events do you want to release" << std::endl;
      value = getdecd(100);
      
      for(int loop = fl1id; loop < (fl1id + value); loop++)
        idgroup.push_back(loop);    
      
      channel->releaseFragment(&idgroup);        
      break;

    case GETINFO:
      std::cout << "Enter the array index (0.." << FM_POOL_NUMB-1 << ")" << std::endl;
      num = getdec();
      if (fm[num] == 0)
      {
        std::cout << "This array element has not yet been filled"  << std::endl;
        break;
      }
      info = fm[num]->getInfo();
      std::cout << "Dumping Config object:" << std::endl;
      info->dump();

      break;

    case HELP:
      std::cout <<  " Exerciser program for the DummyEventManager Class." << std::endl;
      std::cout <<  " The TIMING commands allows to measure the timing of the" << std::endl;
      std::cout <<  " the basic EDummyEventManager methods: this is a bit for specialists ..." << std::endl;
      break;

    case DEBUG:
      {
	std::cout << "Enter the debug level: " << std::endl;
	unsigned int dblevel = getdecd(0);
	std::cout << "Enter the debug package: "<< std::endl;
	unsigned int dbpackage = getdecd(DFDB_ROSFM);
	DF::GlobalDebugSettings::setup(dblevel, dbpackage);
      }
      break;

    case QUIT:
      quit_flag = 1;
      break;

    default:
      std::cout <<  "not implemented yet" << std::endl;
 
    } //main switch
  } while (quit_flag == 0);
  return 0;
}
