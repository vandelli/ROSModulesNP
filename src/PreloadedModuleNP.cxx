// Preloaded Readout module using NPRequestDescriptors

#include <pthread.h>
#include <cstdint>
#include <map>
#include <set>
#include <string>
#include <thread>
#include <vector>
#include <sys/stat.h>

#include "DFdal/PreloadedModuleNP.h"
#include "DFdal/InputChannel.h"
#include "dal/DataFlowParameters.h"
#include "DFdal/DataFile.h"
#include "DFdal/DFParameters.h"

#include "ROSDescriptorNP/DataPage.h"
#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"

#include "ROSInfo/PreloadedInfo.h"

#include "ROSUtilities/ROSErrorReporting.h"
#include "ROSModulesNP/ModulesException.h"

#include "eformat/eformat.h"
#include "eformat/write/eformat.h"
#include "eformat/old/util.h"
#include "EventStorage/pickDataReader.h"

namespace ROS {
   class PreloadedModuleNP : public NPReadoutModule {
   public:
      PreloadedModuleNP();
      virtual ~PreloadedModuleNP() noexcept;

      virtual void configure(const daq::rc::TransitionCmd&);
      virtual void unconfigure(const daq::rc::TransitionCmd&);
      virtual void prepareForRun(const daq::rc::TransitionCmd&);
      virtual void stopGathering(const daq::rc::TransitionCmd&);

      virtual void disable(const std::vector<std::string>&);
      virtual void enable(const std::vector<std::string>&);

      virtual const std::vector<DataChannel *> *channels();

      virtual void clearInfo();
      ISInfo *getISInfo();

      virtual void requestFragment(NPRequestDescriptor* descriptor);

      virtual void clearFragments(std::vector<uint32_t>& level1Ids);

      virtual void registerChannels(unsigned int&,
                                    std::vector<tbb::concurrent_bounded_queue<DataPage*>* >&,
                                    std::vector<std::vector<uint32_t> >&);
      virtual void getL1CountPointers(std::vector<unsigned int*>&,
                                      std::vector<bool*>&);

  private:

      void unpackFragments(const eformat::FullEventFragment<const uint32_t*>& event);
      void loadFile(const std::string& fileSpec);
      void run();

      void emptyFragment(unsigned int rolId, unsigned int l1id,
                         unsigned int statusWord, unsigned int* address);

      typedef std::map<unsigned int, uint32_t*> EventMap;
      std::map<unsigned int, EventMap> m_store;
      std::set<unsigned int> m_rolIdList;

      unsigned int m_storeSize;
      unsigned int m_maxLevel1;

      bool m_active;
      std::thread* m_thread;

      unsigned int m_maxFragSize;
      unsigned int m_channelCount;
      unsigned int m_latestL1Available;
      unsigned int m_baseIndex;
      bool* m_enabled;
      bool m_fakeL1;

      std::map<unsigned int,unsigned int> m_channelIndex;
      std::map<unsigned int,unsigned int> m_channelMap;
      std::map<std::string,unsigned int> m_channelUIDMap;
      std::vector<unsigned int> m_channelIds;
      tbb::concurrent_bounded_queue<DataPage*> m_pageQueue;
      tbb::concurrent_bounded_queue<NPRequestDescriptor*> m_requestQueue;
      std::vector<DataPage*> m_pages;

      PreloadedInfo m_isInfo;
      unsigned int m_runNumber;
      eformat::write::ROBFragment m_emptyFragment;
   };
}

using namespace ROS;



/***************************************************************************/
PreloadedModuleNP::PreloadedModuleNP() : m_maxFragSize(0),
                                           m_channelCount(0),
                                         m_enabled(0),
                                         m_fakeL1(true) {
   clearInfo();
}

/***************************************************************************/
PreloadedModuleNP::~PreloadedModuleNP() noexcept {
}

/***************************************************************************/
ISInfo* PreloadedModuleNP::getISInfo(){
   auto evIter=m_store.begin();
   m_isInfo.firstEv=evIter->first;
   auto evRIter=m_store.rbegin();
   m_isInfo.lastEv=evRIter->first;
   m_isInfo.nEvents=m_store.size();
   m_isInfo.latestAvailable=m_latestL1Available;

   return &m_isInfo;
}

/***************************************************************************/
void PreloadedModuleNP::configure(const daq::rc::TransitionCmd&) {
   unsigned int nPages;

   const daq::df::PreloadedModuleNP* myDal=
      m_configurationDB->get<daq::df::PreloadedModuleNP>(m_uid);
   m_channelCount=0;
   unsigned int nChans=myDal->get_Contains().size();
   m_channelIds.reserve(nChans);
   for (std::vector<const daq::core::ResourceBase*>::const_iterator channelIter=myDal->get_Contains().begin(); 
        channelIter!=myDal->get_Contains().end(); channelIter++) {
      const daq::df::InputChannel* channelPtr=
         m_configurationDB->cast<daq::df::InputChannel, daq::core::ResourceBase> (*channelIter);
      if (channelPtr==0) {
         std::cerr << "Module " << m_uid
                   << " Contains relationship to something (" << (*channelIter)->class_name()
                   << ") that is not an InputChannel\n";
         continue;
      }
      else {
         if (!(*channelIter)->disabled(*m_partition)) {
            unsigned int channelId=channelPtr->get_Id();
            std::string channelUID=channelPtr->UID();
            //std::cout << "Pushing back rolID " << std::hex << channelId << std::dec
            //          << ", UID=" << channelUID << std::endl;
            m_channelIds.push_back(channelId);
            m_channelUIDMap[channelUID]=m_channelCount;
            m_channelCount++;
         }
      }
      nPages=myDal->get_nPages()*m_channelCount;

   }
   m_rolIdList.insert(m_channelIds.begin(),m_channelIds.end());

   m_enabled=new bool[m_channelCount];
   for (unsigned int chan=0;chan<m_channelCount;chan++) {                                                   
      m_enabled[chan]=true;                                                                                 
   }

   auto fileVec=
      m_configurationDB->cast<daq::df::DFParameters, daq::core::DataFlowParameters>(m_partition->get_DataFlowParameters())
      ->get_UsesDataFiles();
   for (auto fileIter : fileVec) {
      auto fileName=fileIter->get_FileName();
      struct stat statBuf;
      std::string splitDir=fileName+".per-rob";
      if (stat(splitDir.c_str(),&statBuf) != 0) {
         // Per ROL data files not available
         loadFile(fileName);
      }
      else {
         // OK, we load one file per ROL
         for (auto chanIter : m_rolIdList) {
            std::ostringstream fnStream;
            fnStream << splitDir << "/0x" << std::setfill('0') << std::setw(8) << std::uppercase << std::hex << chanIter << ".data";
            loadFile(fnStream.str());
         }
      }
   }

   unsigned int pageSize;
   if (m_fakeL1) {
      // Only needed if we copy the fragments into the page buffers
      pageSize=m_maxFragSize+32; // Add a bit for extra header words
      std::cout << "PreloadedModuleNP::configure() Allocateing " << nPages
                << " pages of " << m_maxFragSize << " bytes ("
                << pageSize << " words)" << std::endl;
   }
   else {
      // Only need enough room for empty fragment
      pageSize=32;
   }
   for (unsigned int page=0;page<nPages;page++) {
      unsigned int* buf=new unsigned int[pageSize];
      // Both virt and phys address set to virt addr of buffer
      m_pages.emplace_back(new DataPage(buf,buf));
      m_pageQueue.push(m_pages[page]);
   }

   auto evIter= m_store.begin();
   auto lastEvIter=m_store.rbegin();
   unsigned int firstEv=evIter->first;
   std::cout  << "Event store contains " <<  m_store.size()
              << " events from " << std::hex << firstEv
              << " to " << lastEvIter->first
              << std::dec << std::endl;
}

/***************************************************************************/
void PreloadedModuleNP::registerChannels(
   unsigned int& baseIndex,
   std::vector<tbb::concurrent_bounded_queue<DataPage*>* > & pageQueueVec,
   std::vector<std::vector<uint32_t> >& channelIdVec) {

   m_baseIndex=baseIndex;
   channelIdVec.push_back(m_channelIds);
   pageQueueVec.push_back(&m_pageQueue);
   for (auto id : m_channelIds) {
      m_channelIndex[id]=baseIndex;
      m_channelMap[baseIndex]=id;
      baseIndex++;
   }
}

/***************************************************************************/
void PreloadedModuleNP::getL1CountPointers(std::vector<unsigned int*>& countPointers,
                                          std::vector<bool*>& enabled) {
   for (unsigned int chan=0;chan<m_channelIds.size();chan++) {
      countPointers.push_back(&m_latestL1Available);
      enabled.push_back(&m_enabled[chan]);
   }
}

/***************************************************************************/
void PreloadedModuleNP::prepareForRun(const daq::rc::TransitionCmd&) {
   m_runNumber=m_runParams->getInt("runNumber");
   m_emptyFragment.rod_run_no(m_runNumber);
   m_latestL1Available=0xfffffffe;
   clearInfo();
   m_active=true;
   m_thread=new std::thread(&PreloadedModuleNP::run,this);
   pthread_setname_np(m_thread->native_handle(),"PreloadedModule");

}

/***************************************************************************/
void PreloadedModuleNP::stopGathering(const daq::rc::TransitionCmd&) {
   m_active=false;
   m_requestQueue.abort();
   std::cout << "PreloadedModuleNP::stopGathering() Waiting for thread to exit\n";
   m_thread->join();
   delete m_thread;
   m_thread=0;
}

/***************************************************************************/
void PreloadedModuleNP::unconfigure(const daq::rc::TransitionCmd&) {
   if (m_enabled!=0) {
      delete m_enabled;
      m_enabled=0;
   }

   for (auto page : m_pages) {
      // The physical address points to our buffer the virtual may point to
      // the original data in the index
      delete page->physicalAddress();
      delete page;
   }

   for (auto evIter : m_store) {
      for (auto robIter : evIter.second) {
         delete robIter.second;
      }
   }
   m_store.clear();
   m_storeSize=0;
   m_pages.clear();
   m_pageQueue.clear();
   m_channelIds.clear();
}


/***************************************************************************/
void PreloadedModuleNP::disable(const std::vector<std::string>& argVec){
   uint64_t disabledCount=0;
   for (std::string arg: argVec) {
      if (m_channelUIDMap.find(arg) != m_channelUIDMap.end()) {
         unsigned int chan=m_channelUIDMap[arg];
         m_enabled[chan]=false;
         disabledCount++;
         std::cout << "disabling channel " <<chan << ", UID " << arg << std::endl;
      }
   }

   for (unsigned int chan=0;chan<m_channelCount;chan++) {
      std::cout << "channel " << chan << " enabled=" << m_enabled[chan] << std::endl;
   }
}

/***************************************************************************/
void PreloadedModuleNP::enable(const std::vector<std::string>& argVec){
   uint64_t enabledCount=0;
   for (std::string arg: argVec) {
      if (m_channelUIDMap.find(arg) != m_channelUIDMap.end()) {
         unsigned int chan=m_channelUIDMap[arg];
         m_enabled[chan]=true;
         enabledCount++;
         std::cout << "enabling channel " <<chan << ", UID " << arg << std::endl;
      }
   }
   for (unsigned int chan=0;chan<m_channelCount;chan++) {
      std::cout << "channel " << chan << " enabled=" << m_enabled[chan] << std::endl;
   }
}

/***************************************************************************/
void PreloadedModuleNP::clearInfo(void)
{
   m_isInfo.latestAvailable=0;
   m_isInfo.latestRequested=0;
   m_isInfo.nMissing=0;
   m_isInfo.nIncomplete=0;
   m_isInfo.nRequested=0;
}

/***************************************************************************/
const std::vector<DataChannel *> *PreloadedModuleNP::channels(void) {
   return 0;
}


/***************************************************************************/
void PreloadedModuleNP::requestFragment(NPRequestDescriptor* descriptor){
   m_isInfo.latestRequested=descriptor->level1Id();
   descriptor->expectReply();
   m_requestQueue.push(descriptor);
}

/***************************************************************************/
void PreloadedModuleNP::clearFragments(std::vector<uint32_t>& level1Ids){
//   m_latestL1Available+=level1Ids.size();
}

void PreloadedModuleNP::emptyFragment(unsigned int rolId,
                                      unsigned int l1id,
                                      unsigned int statusWord,
                                      unsigned int* address) {
   m_emptyFragment.source_id(rolId);
   m_emptyFragment.status(1,&statusWord);
   m_emptyFragment.rod_lvl1_id(l1id);
   auto nodePtr=m_emptyFragment.bind();
   eformat::write::copy(*nodePtr,
                        address,
//                        robFragment.size_word());
                        m_emptyFragment.size_word());

}
/***************************************************************************/
void PreloadedModuleNP::run() {
   while (m_active) {
      NPRequestDescriptor* descriptor;
      try {
         m_requestQueue.pop(descriptor);

         auto channelList=descriptor->localIds();

         unsigned int fakeLevel1Id=descriptor->level1Id();
         if (m_fakeL1) {
            fakeLevel1Id=fakeLevel1Id%(m_maxLevel1+1);
            // std::cout << "Looking for fake L1 " << std::hex << fakeLevel1Id
            //           << " for requested " << descriptor->level1Id()
            //           << std::dec << std::endl;
         }
         bool wanted=false;
         auto eventIter = m_store.find(fakeLevel1Id);
         if (eventIter == m_store.end()) {
            // std::cout << "Event not found, creating empty fragment(s)\n";
            // Fragment not found 
            unsigned int statusWord=0x40000000;
            if (descriptor->level1Id()<m_latestL1Available) {
               statusWord=0x20000000;
            }
            for (unsigned int channel : *channelList) {
               auto chIter=m_channelMap.find(channel);
               if (chIter==m_channelMap.end()){
                  // Assume channel belongs to another Module, skip it
                  continue;
               }
               unsigned int rolId=chIter->second;
               if (m_rolIdList.find(rolId)==m_rolIdList.end()) {
                  std::cerr << "Can't find rol " << std::hex << rolId
                            << " in our list of ROLs" << std::dec << std::endl;
                  continue;
               }
               wanted=true;
               // Create empty event header
               DataPage* page;
               m_pageQueue.pop(page);
               descriptor->assignPage(channel,page);
               unsigned int* data=page->physicalAddress();
               page->virtualAddress(data);
               emptyFragment(rolId,descriptor->level1Id(),statusWord,data);
            }
            if (wanted) {
               m_isInfo.nMissing++;
            }
         }
         else {
            bool complete=true;
            auto eventMap=&(*eventIter).second;
            for (unsigned int channel : *channelList) {
               auto chIter=m_channelMap.find(channel);
               if (chIter==m_channelMap.end()){
                  // Assume channel belongs to another Module, skip it
                  continue;
               }
               unsigned int rolId=chIter->second;
#if 0
               // Is this check overkill?
               if (m_rolIdList.find(rolId)==m_rolIdList.end()) {
                  std::cerr << "Can't find rol " << std::hex << rolId
                            << " in our list of ROLs" << std::dec << std::endl;
                  continue;
               }
#endif
               wanted=true;
               unsigned int* data;
               DataPage* page;
               m_pageQueue.pop(page);
               descriptor->assignPage(channel,page);
               data=page->physicalAddress();
               auto rolIter=eventMap->find(rolId);
               if (rolIter == eventMap->end()) {
                  page->virtualAddress(data);
                  complete=false;
                  // No fragment found for this ROL
                  // std::cout << "ROL not found, creating empty fragment\n";
                  unsigned int statusWord=0x40000000;
                  if (descriptor->level1Id()<m_latestL1Available) {
                     statusWord=0x20000000;
                  }
                  emptyFragment(rolId,descriptor->level1Id(),statusWord,data);
               }
               else {
                  if (m_fakeL1) {
//                     std::cout << "Event found OK, copying fragment\n";
                     unsigned int* frag=rolIter->second;
                     memcpy((void*)data,(void*)frag,frag[1]*4);
                  }
                  else {
                     data=rolIter->second;
                     page->virtualAddress(data);
                  }
                  unsigned int hsize=data[2];
                  if(data[hsize+4]!=m_runNumber) {
                     data[hsize+4]=m_runNumber;
                  }
                  if (data[hsize+5]!=descriptor->level1Id()) {
                     data[hsize+5]=descriptor->level1Id();
                  }
               }
            }
            if (wanted && !complete) {
               m_isInfo.nIncomplete++;
            }
         }
         if (wanted){
            m_isInfo.nRequested++;
         }
         m_collectorQueue->push(descriptor);
      }
      catch (tbb::user_abort& abortException) {
         break;
      }
   }
}



void PreloadedModuleNP::loadFile(const std::string& fileSpec)
{
   std::cout << "Loading data from " << fileSpec << std::endl;
   // Check that the input file exists.  FileStorage would assume its
   // fileName argument is an output file  if it doesn't exist i.e. we don't
   // get an error from it.
   struct stat statBuf;
   if (stat(fileSpec.c_str(),&statBuf) != 0) {
      CREATE_ROS_EXCEPTION(ex1,ModulesException,NOFILE,fileSpec);
      throw(ex1);
      std::cerr << "Error, can't find data file " << fileSpec << std::endl;
      return;
   }

   DataReader* reader=pickDataReader(fileSpec);
   while (reader->good()) {
      unsigned int eventSize;
      char* buffer;
      DRError errorCode = reader->getData(eventSize,&buffer);
      if (errorCode==DROK) {
         //std::cout << "read event size " << eventSize << std::endl;
         unsigned int* header=reinterpret_cast<uint32_t*>(buffer);
         if (header[0]==0xaa1234aa) {
            if (eformat::peek_major_version(header)!=(eformat::DEFAULT_VERSION>>16)) {
               unsigned int* newbuf=new unsigned int[eventSize/4];
               eformat::old::convert(header,newbuf,eventSize/4,eformat::CRC16_CCITT);
               delete [] buffer;
               buffer=(char*)newbuf;
               header=newbuf;
            }
            const eformat::FullEventFragment<const uint32_t*> fragment(header);
            //std::cout << "Calling unpackFragments for event " << fragment.lvl1_id() << std::endl;
            unpackFragments(fragment);
         }
         else {
            std::cerr <<  "PreloadedModuleNP::loadFile(" << fileSpec << ")"
                      << "  bad header marker " << std::hex << header[0] << std::dec
                      << std::endl;
            //ers::error(ex);
         }
      }
      else {
         std::cerr <<  "PreloadedModuleNP::loadFile(" << fileSpec << ")"
                   << " error " << errorCode << " from reader->getData()"
                   << std::endl;
      }
      delete [] buffer;
   }
   delete reader;
}

//***************************************************************************
void PreloadedModuleNP::unpackFragments(
   const eformat::FullEventFragment<const uint32_t*>& event) {

   uint32_t level1Id = event.lvl1_id();
   if (level1Id > m_maxLevel1) {
      m_maxLevel1=level1Id;
   }

   auto storeIter=m_store.find(level1Id);
   if (storeIter==m_store.end()) {
      ///Adds a new entry with the new L1 identifier
      m_store[level1Id]=EventMap();
   }

   int nRobsUsed=0;
   //std::cout << "Processing l1Id " << std::hex << level1Id;
   for (unsigned int rob=0; rob<event.nchildren(); rob++) {
      const uint32_t* robp;
      event.child(robp, rob);
      eformat::ROBFragment<const uint32_t*> robFragment(robp);

      //gets (presumably unique) source identifer from ROB
      unsigned int  rolId=robFragment.source_id();

      // Am I interested on this ROB identifier?
      if (m_rolIdList.find(rolId)!= m_rolIdList.end()) {
         //std::cout << " adding ROL " << rolId;
         nRobsUsed++;
         auto rolIter=m_store[level1Id].find(rolId);
         //if I already have an entry there...
         if (rolIter!=m_store[level1Id].end()) {
            //CREATE_ROS_EXCEPTION(ex, ModulesException, DUPLICATE_ROBID, rolId << " for l1 id " << level1Id);
            //ers::warning(ex);
            continue;
         }

         //  Now I need to store the data somewhere and keep a
         //  pointer to it in s_store
         uint32_t* storage=new uint32_t[robFragment.fragment_size_word()];
         memcpy(storage, robp, robFragment.fragment_size_word()*4);
         m_store[level1Id][rolId]=storage;
         m_storeSize+=robFragment.fragment_size_word()*4;
         if (robFragment.fragment_size_word()>m_maxFragSize) {
            m_maxFragSize=robFragment.fragment_size_word();
         }
      }
      else {
         //DEBUG_TEXT(DFDB_ROSFM, 20, "Not using data from ROL " << std::hex << rolId << " (" << std::dec << rolId << ")" );
      }
   }
   //std::cout << std::dec << ", saved data from " << nRobsUsed << " ROBs" << std::endl;
}




/***************************************************************************/
//FOR THE PLUGIN FACTORY
extern "C" {
   extern ReadoutModule* createPreloadedModuleNP();
}
ReadoutModule* createPreloadedModuleNP(){
   return (new PreloadedModuleNP());
}
