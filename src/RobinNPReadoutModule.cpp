/*********************************************/
/*  ATLAS ROS Software 			 		     */
/*							                 */
/*  Class: RobinNPReadoutModule 			 */
/*  Author: William Panduro Vazquez 	RHUL */
/*			(j.panduro.vazquez@cern.ch)		 */
/*********************************************/

#include <new>
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "DFSubSystemItem/ConfigException.h"
#include "ROSModulesNP/ModulesException.h"
#include "ROSModulesNP/RobinNPReadoutModule.h"
#include "ROSModulesNP/DDTScheduledUserAction.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "ROSEventFragment/ROBFragment.h"

using namespace ROS;

bool RobinNPReadoutModule::s_firstModuleFound = false;
bool RobinNPReadoutModule::s_DDTCreated = false;

/******************************************/
RobinNPReadoutModule::RobinNPReadoutModule(void)
/******************************************/
: m_memoryPool(0),
  m_robIn(0),
  m_physicalAddress(0),
  m_numberOfDataChannels(0),
  m_firstRolPhysicalAddress(0),
  m_maxTickets(0),
  m_DDTscheduledUserAction(0)
{ 
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::Constructor: called");
}


/***************************************/
RobinNPReadoutModule::~RobinNPReadoutModule() noexcept
		/***************************************/
		{
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::Destructor: called");
	if (m_robIn != 0)
	{
		std::vector <DataChannel *>::iterator rols;
		for (rols = m_dataChannels.begin(); rols != m_dataChannels.end(); rols++)
			delete (*rols);

		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::Destructor: m_robIn is at " << HEX(m_robIn));
		delete m_robIn;
		m_robIn = 0;
	}

	if (m_memoryPool != 0)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::Destructor: m_memoryPool is at " << HEX(m_memoryPool));
		delete m_memoryPool;
		m_memoryPool = 0;
	}

	if (m_DDTscheduledUserAction != 0)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::Destructor: m_DDTscheduledUserAction is at " << HEX(m_DDTscheduledUserAction));
		delete m_DDTscheduledUserAction;
		m_DDTscheduledUserAction = 0;
		s_firstModuleFound = false;
		s_DDTCreated = false;
	}

		}


/********************************************************************/
void RobinNPReadoutModule::setup(DFCountedPointer<Config> configuration)
/********************************************************************/
{
	// Get the configuration parameters here
	m_configuration = configuration;
}


/******************************************/
void RobinNPReadoutModule::prepareForRun(const daq::rc::TransitionCmd& /*cmd*/)
/******************************************/
{ 
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::prepareForRun: called");

	if (m_numberOfDataChannels)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::prepareForRun: Now resetting m_memoryPool");
		m_memoryPool->reset();
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::prepareForRun: Reset of m_memoryPool OK");
	}

	m_robIn->resetSubRobChannels();

	//Nothing to be done at this level but the data channels should enable the S-Links
	for (u_int chan = 0; chan < m_numberOfDataChannels; chan++){

		RobinNPDataChannel *dataChannel = dynamic_cast<RobinNPDataChannel *>(m_dataChannels[chan]);
		if(dataChannel != 0){
			dataChannel->prepareForRun();
		}
		else{
			CREATE_ROS_EXCEPTION(exnppreprun1, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::prepareForRun: dynamic cast to RobinNPDataChannel failed");
			throw(exnppreprun1);
		}

	}

	// start ECR scanning on enabled links
	if (m_DDTscheduledUserAction != 0){
		DDTScheduledUserAction* action = dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction);
		if(action != 0){
			action->prepareForRun();
		}
		else{
			CREATE_ROS_EXCEPTION(exnppreprun2, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::prepareForRun: dynamic cast to DDTScheduledUserAction failed");
			throw(exnppreprun2);
		}
	}

	if(!m_robIn->getThreadState()) {
		m_robIn->startThreads();
	}

	if(!m_robIn->getLegacyStatus()){
		if(m_maxTickets != 0){
			initTicketing(m_maxTickets);
		}
		else{
			CREATE_ROS_EXCEPTION(exnp100a, ModulesException, PCIROBIN_NOTICKETS , "RobinNPReadoutModule::prepareForRun: number of available tickets is zero");
			throw(exnp100a);
		}
	}
}


/***********************************/
void RobinNPReadoutModule::stopGathering(const daq::rc::TransitionCmd& /*cmd*/)
/***********************************/
{ 
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::stopEB: called");

	// stop ECR scanning on enabled links
	if (m_DDTscheduledUserAction !=0){
		DDTScheduledUserAction* action = dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction);
		if(action != 0){
			action->stopEB();
		}
		else{
			CREATE_ROS_EXCEPTION(exnpstopeb1, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::stopGathering: dynamic cast to DDTScheduledUserAction failed");
			throw(exnpstopeb1);
		}
	}

	//Nothing to be done at this level but the data channels should disable the S-Links
	for (u_int chan = 0; chan < m_numberOfDataChannels; chan++){
		RobinNPDataChannel *dataChannel = dynamic_cast<RobinNPDataChannel *>(m_dataChannels[chan]);
		if(dataChannel != 0){
			dataChannel->stopFE();
		}
		else{
			CREATE_ROS_EXCEPTION(exnpstopeb2, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::stopGathering: dynamic cast to RobinNPDataChannel failed");
			throw(exnpstopeb2);
		}

	}

}


/**************************************/
void RobinNPReadoutModule::configure(const daq::rc::TransitionCmd& /*cmd*/)
/**************************************/
{ 
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::configure: called");
	u_int npages = 0;
	u_int pageSize = 0;
	m_maxTickets = 0;

	m_configuration->dump();

	try
	{
		//Get the configuration variables for the whole board
		m_numberOfDataChannels = m_configuration->getInt("numberOfChannels");
		if (m_numberOfDataChannels == 0 || m_numberOfDataChannels > s_maxRols)
		{
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: zero or invalid number m_numberOfDataChannels = " << m_numberOfDataChannels);
			return;
		}

		m_physicalAddress      = m_configuration->getUInt("PhysicalAddress");  //This identifies a CARD not a ROL!!!

		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: m_numberOfDataChannels = " << m_numberOfDataChannels);
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: m_physicalAddress      = " << m_physicalAddress);

		//This table links the physical addresses of the ROLs on a ROBIN to the channel numbers. The value "19" means "invalid"
		for(unsigned int rol = 0; rol < s_maxRols; ++rol){
			m_rolPhysicalAddressTable[rol] = 19;
		}

		for (u_int chan = 0; chan < m_numberOfDataChannels; chan++)
		{
			npages += m_configuration->getInt("memoryPoolNumPages", chan);

			u_int channelPageSize = m_configuration->getInt("memoryPoolPageSize", chan);
			if ((chan > 0) && (pageSize != channelPageSize))
			{
				CREATE_ROS_EXCEPTION(ex102, ModulesException, PCIROBIN_PSIZE, "RobinNPReadoutModule::configure: Page size was not the same for all channels in configuration, using the maximum");
				ers::warning(ex102);
			}
			if (channelPageSize > pageSize)
				pageSize = channelPageSize;
		}
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: npages                 = " << npages);
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: pageSize               = " << pageSize);

		m_robIn = new RobinNP(m_physicalAddress);
		m_robIn->init();

		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: m_robIn is at " << HEX(m_robIn));

		if(npages > m_robIn->getNumDMAPages()){
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: number of memory pool pages exceeds allocated maximum.");
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: requested " << npages << " pages but only " << m_robIn->getNumDMAPages() << " available.");
			CREATE_ROS_EXCEPTION(exnp100, ModulesException, PCIROBIN_PSIZE , "RobinNPReadoutModule::configure: number of memory pool pages exceeds allocated maximum. Requested " << npages << " pages but only " << m_robIn->getNumDMAPages() << " available.");
			throw(exnp100);
		}
		m_maxTickets+=npages;
		// Create a memoryPool structure around the event buffer
		unsigned char **eventBase       = m_robIn->getVirtArray();
		unsigned long *physicalAddress = m_robIn->getPhysArray();
		m_memoryPool = new WrapperMemoryPoolNP(npages, pageSize, eventBase, physicalAddress);
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: m_memoryPool is at " << HEX(m_memoryPool));

		if(m_numberOfDataChannels > m_robIn->getNumRols()){
			CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_TOOMANYCHANNELS, "ERROR: Configuration includes " << m_numberOfDataChannels << " ROLs but RobinNP has a maximum of " << m_robIn->getNumRols());
			throw(ex1);
		}

		std::set<u_int> roblist;
		std::set<std::string> uidlist;
		// Now it is time to create the Data Channels
		for (u_int i = 0; i < m_numberOfDataChannels; i++)
		{
			u_int rolId = m_configuration->getInt("Id", i);
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: Assembled rolId = " << HEX(rolId));

			u_int rolPhysicalAddress = m_configuration->getInt("ROLPhysicalAddress", i);
			std::string uid = m_configuration->getString("UID", i);
			m_rolPhysicalAddressTable[i] = rolPhysicalAddress;

			if(i != 0){
				for(u_int index = 0; index < i; index++){
					if(uidlist.find(uid) != uidlist.end()){
						CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATEROBUID, "ERROR: Configuration includes ROB UID " << uid.c_str() << " more than once");
						throw(ex1);
					}
					if(m_rolPhysicalAddressTable[index] == rolPhysicalAddress){
						CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATECHANNEL, "ERROR: Configuration includes ROL " << rolPhysicalAddress << " more than once");
						throw(ex1);
					}
					if(roblist.find(rolId) != roblist.end()){
						CREATE_ROS_EXCEPTION(ex1, ModulesException, ROBINNP_DUPLICATEROBID, "ERROR: Configuration includes ROB " << rolId << " more than once");
						throw(ex1);
					}
				}
			}

			roblist.insert(rolId);
			uidlist.insert(uid);
			// first data channel in the configuration
			if (!s_firstModuleFound && i == 0)
			{
				m_firstRolPhysicalAddress = rolPhysicalAddress;
				s_firstModuleFound = true;
			}
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: rolId of Channel " << i << " = " << rolId);
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: rolPhysicalAddress of Channel " << i << " = " << rolPhysicalAddress);
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: Creating channel");

			printf("Initialising RobinNPDataChannel with rolId %i and physical address %i - data channel index %i\n",rolId,rolPhysicalAddress,i);
			RobinNPDataChannel *channel = new RobinNPDataChannel(rolId, i, rolPhysicalAddress, m_robIn->getRol(rolPhysicalAddress), m_physicalAddress, m_memoryPool);

			//Get the UID of the DataChannel. We need it if we want to enable / disable a ROL on the fly

			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: UID of the current channel = " << uid);
			m_uidMap[uid] = channel;

			//CRC stuff
			u_int crc_interval = m_configuration->getInt("CrcCheckInterval", i);
			printf("Getting ROL %i\n",i);
			m_robIn->getRol(rolPhysicalAddress).setCRCInterval(crc_interval);

			m_dataChannels.push_back(channel);
		}

		// Check the BIST result and the PPC application
		RobinNPStats *status = m_robIn->getGeneralStatistics();

		// Do we accept the FPGA F/W?
		u_int fwrevision_number = m_configuration->getUInt("FPGAVersion");
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: revision_number: 0x" << HEX(fwrevision_number));
		if (fwrevision_number != (u_int)status->m_firmwareVersion)
		{
			DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPReadoutModule::configure: The FPGA F/W of the RobinNP does not have the required version");
			DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPReadoutModule::configure: Configuration expects = 0x" << HEX(fwrevision_number) << " but FPGA has = 0x" << HEX((u_int)status->m_firmwareVersion));
			CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_OLDFPGAFW, "ERROR: RobinNP (physical Address = "
					<< m_physicalAddress << ") is out of date: Expected = 0x" << HEX(fwrevision_number) << "  Found = 0x"
					<< HEX((u_int)status->m_firmwareVersion));
			throw(ex1);
		}

		RobinNPConfig configuration;
		configuration.setMaxRxPages(m_configuration->getUInt("MaxRxPages"));
		configuration.setPageSize(m_configuration->getUInt("Pagesize"));
		configuration.setCRCCheckInterval(m_configuration->getInt("CrcCheckInterval", 0));
		//Check parameters for consistency
		unsigned int maxfragsize = 4 * configuration.getPageSize() * configuration.getMaxRxPages();  //in bytes
		if (maxfragsize > pageSize)
		{
			CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_PSIZE, "The ROBIN can produce events of up to " << maxfragsize
					<< " bytes. But the ROS has a limit of " << pageSize << " bytes");
			throw(ex1);
		}

		for (u_int channelIndex = 0; channelIndex < m_numberOfDataChannels; ++channelIndex)
		{
			if (m_rolPhysicalAddressTable[channelIndex] > 11)  //Well, this should never happen.....
			{
				DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPDescriptorReadoutModule::configure: I don't have a physical address for data channel # " << channelIndex);
				CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_NOROLPA, "ERROR: There is no ROL physical address for data channel # "
						<< channelIndex << " on ROBIN " << m_physicalAddress);
				throw(ex1);
			}

			try
			{
				RobinNPROLConfig rolConfig;
				rolConfig.setDiscardMode(false); //check DB entry for this
				rolConfig.setRobId(m_configuration->getInt("Id",channelIndex));
				rolConfig.setRolDataGen(m_configuration->getUInt("RolDataGen",channelIndex));
				rolConfig.setRolEnabled(true);
				rolConfig.setTestSize(m_configuration->getUInt("TestSize",channelIndex));
				configuration.configureRol(rolConfig,m_rolPhysicalAddressTable[channelIndex]);
			}
			catch(std::exception ex)
			{
				std::cout << "DFConfiguration Exception " << ex.what() << std::endl;
			}
		}

		m_robIn->configure(configuration);
	}
	catch (std::exception & mess)
	{
		DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPReadoutModule::configure: Exception received: " << mess.what());
		throw;
	}

	// create the DDT User Action
	bool triggerQueueFlag = m_configuration->getBool("triggerQueue");
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: triggerQueue = " << triggerQueueFlag);
	if (triggerQueueFlag && s_firstModuleFound && !s_DDTCreated)
	{
		u_int triggerQueueSize = m_configuration->getInt("triggerQueueSize");
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: triggerQueueSize = " << triggerQueueSize);

		u_int triggerQueueUnblockOffset = m_configuration->getInt("triggerQueueUnblockOffset");
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: triggerQueueUnblockOffset = " << triggerQueueUnblockOffset);

		int deltaTimeMs = m_configuration->getInt("RobinProbeInterval");
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::configure: deltaTimeMs = " << deltaTimeMs);
		m_DDTscheduledUserAction = new DDTScheduledUserAction(m_robIn, m_firstRolPhysicalAddress, deltaTimeMs, triggerQueueSize, triggerQueueUnblockOffset);
		std::cout << "RobinNPReadoutModule::configure: Created DDTScheduledUserAction" << " for Module # " << m_physicalAddress << " channel = " << m_firstRolPhysicalAddress << std::endl;
		s_DDTCreated = true;
	}
	printf("Robin configuration complete!\n");
}


/****************************************/
void RobinNPReadoutModule::unconfigure(const daq::rc::TransitionCmd& /*cmd*/)
/****************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::unconfigure: Called");

	//Release the memory pool
	if (m_memoryPool != 0)
	{
		delete m_memoryPool;
		m_memoryPool = 0;
	}

	//Release the Robin
	if (m_robIn != 0)
	{
		std::vector <DataChannel *>::iterator rols;
		for (rols = m_dataChannels.begin(); rols != m_dataChannels.end(); rols++)
			delete (*rols);

		m_dataChannels.clear();
		delete m_robIn;
		m_robIn = 0;
	}

	//Delete the DDT User Action Scheduler
	if (m_DDTscheduledUserAction != 0)
	{
		delete m_DDTscheduledUserAction;
		m_DDTscheduledUserAction = 0;
		s_firstModuleFound = false;
		s_DDTCreated = false;
	}

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::unconfigure: Done");
}



//Note: "disable" in this context means "disable channel" and therefore "enable discard mode"
/************************************************/
void RobinNPReadoutModule::disable(const std::vector<std::string>& channelList)
/************************************************/
{
	for (auto uid : channelList) {
		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::disable: Called with UID = " << uid);

		std::map<std::string, RobinNPDataChannel *>::iterator it;
		it = m_uidMap.find(uid);
		if (it == m_uidMap.end())
		{
			DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::disable: Cannot find data chanel for UID = " << uid);
		}
		else
		{
			RobinNPDataChannel *dchannel = (*it).second;
			dchannel->enable();  //Enable discard mode
		}

		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::disable: Done");
	}
}

/***********************************************************************************/
void RobinNPReadoutModule::user(const daq::rc::UserCmd& command)
/***********************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::userCommand: Called with commandName = " << command.commandName());

	if((command.commandName() == "clear") || (command.commandName() == "CLEAR")) {
		for (auto rol_id : command.commandParameters()) {
			std::map<std::string, RobinNPDataChannel *>::iterator it;
			it = m_uidMap.find(rol_id);
			if (it == m_uidMap.end())
			{
				DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::userCommand: Cannot find data channel for UID = " << rol_id);
			}
			else
			{
				RobinNPDataChannel *dchannel = (*it).second;
				SEND_ROS_INFO("Restarting ROL 0x" << HEX(dchannel->id()));
				dchannel->restart();
				DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPReadoutModule::userCommand: Channel " << rol_id << " has been reset");
			}
		}
	}
	else {
		DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPReadoutModule::userCommand:Illegal command received");
		return;
	}


	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::userCommand: Done");
}

//Note: "enable" in this context means "enable channel" and therefore "disable discard mode"
/***********************************************/
void RobinNPReadoutModule::enable(const std::vector<std::string>& channelList)
/***********************************************/
{
	for (auto uid : channelList) {
		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::enable: Called with UID = " << uid);

		std::map<std::string, RobinNPDataChannel *>::iterator it;
		it = m_uidMap.find(uid);
		if (it == m_uidMap.end())
		{
			DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPReadoutModule::enable: Cannot find data chanel for UID = " << uid);
		}
		else
		{
			RobinNPDataChannel *dchannel = (*it).second;
			dchannel->disable();  //disable discard mode
		}

		DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::enable: Done");
	}
}


/***************************************************************/
const std::vector<DataChannel *> * RobinNPReadoutModule::channels()
/***************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::channels: Called");
	return &m_dataChannels;
}


/**********************************/
void RobinNPReadoutModule::clearInfo()
/**********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::clearInfo: Entered");

	if (m_DDTscheduledUserAction != 0){
		DDTScheduledUserAction* action = dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction);
		if(action != 0){
			action->clearInfo();
		}
		else{
			CREATE_ROS_EXCEPTION(exnpclearinfo, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::clearInfo: dynamic cast to DDTScheduledUserAction failed");
			throw(exnpclearinfo);
		}
	}
}


/**************************************************/
void RobinNPReadoutModule::publishFullStatistics(void)
/**************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::publishFullStatistics: Entered");

	if (m_DDTscheduledUserAction !=0){
		DDTScheduledUserAction* action = dynamic_cast<DDTScheduledUserAction *>(m_DDTscheduledUserAction);
		if(action != 0){
			action->printTimeHistogram();
		}
		else{
			CREATE_ROS_EXCEPTION(exnppubfullstat, ModulesException, ROBINNP_BADCAST , "RobinNPReadoutModule::publishFullStatistics: dynamic cast to DDTScheduledUserAction failed");
			throw(exnppubfullstat);
		}
	}

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPReadoutModule::publishFullStatistics: Done");
}


/******************************************************************/
std::string RobinNPReadoutModule::getBistErrorString(u_int /*bistStatus*/)
/******************************************************************/
{
	std::ostringstream errorMessages;

	/*	if ((bistStatus & bistDesIdWrong) != 0)
		errorMessages << "FPGA design ID wrong";

	if ((bistStatus & bistRolTlkPrbs) != 0)
		errorMessages << "Error Code: bistRolTlkPrbs";

	if ((bistStatus & bistRolTlkLoop) != 0)
		errorMessages << "Error Code: bistRolTlkLoop";

	if ((bistStatus & bistBufAddrError) != 0)
		errorMessages << "Error Code: bistBufAddrError";

	if ((bistStatus & bistBufDataError) != 0)
		errorMessages << "Error Code: bistBufDataError";

	if ((bistStatus & bistParmError) != 0)
		errorMessages << "Error Code: bistParmError";
	 */
	return errorMessages.str();
}

void RobinNPReadoutModule::initTicketing(unsigned int numTickets){
	m_robIn->initialiseLegacyControl(numTickets);
}

unsigned int RobinNPReadoutModule::getNumSubRobs(){
	return m_robIn->getNumSubRobs();
}

unsigned int RobinNPReadoutModule::getNumRolsPerSubRob(){
	return m_robIn->getNumChannelsPerSubRob();
}

//FOR THE PLUGIN FACTORY
extern "C" 
{
extern ReadoutModule* createRobinNPReadoutModule();
}
ReadoutModule* createRobinNPReadoutModule()
{
	return (new RobinNPReadoutModule());
}

