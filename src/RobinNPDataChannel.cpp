/*********************************************/
/*  ATLAS ROS Software 			 		     */
/*							                 */
/*  Class: RobinNPDataChannel 			 	 */
/*  Author: William Panduro Vazquez		RHUL */
/*			(j.panduro.vazquez@cern.ch)		 */
/*********************************************/

#include "ROSModulesNP/RobinNPDataChannel.h"
#include "ROSModulesNP/ModulesException.h"
#include "ROSRobinNP/RolNP.h"
#include "ROSEventFragment/ROBFragment.h"
#include "ROSBufferManagement/Buffer.h"
#include "DFThreads/DFThread.h"
#include "DFSubSystemItem/Config.h"
#include "DFSubSystemItem/ConfigException.h"
#include "DFDebug/DFDebug.h"
#include "ROSUtilities/ROSErrorReporting.h"
#include "rcc_time_stamp/tstamp.h"
#include "ROSRobinNP/RobinNPHardware.h"
#include "ROSMemoryPool/MemoryPage.h"

using namespace ROS;

/*********************************************************************************/
RobinNPDataChannel::RobinNPDataChannel(u_int id, u_int configId, u_int physicalAddress,
		RolNP & rol_ptr, u_int RobinphysicalAddress,
		WrapperMemoryPoolNP *mpool) :
		/*********************************************************************************/
		  DataChannel(id, configId, physicalAddress),
		  m_memoryPool(mpool),
		  m_rol(rol_ptr),
		  m_requestMutex(0),
		  m_gcdeleted(0),
		  m_gcfree(0),
		  m_id(id),
		  m_l1id_modulo(0),
		  m_RobinphysicalAddress(RobinphysicalAddress),
		  m_numberOfLevel1Last(0)
{
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::constructor: Called");

	m_statistics.numberOfMayCome      = 0;
	m_statistics.numberOfNeverToCome  = 0;
	m_statistics.numberOfNotDeleted   = 0;
	m_statistics.robId                = id;
	m_statistics.pagesFree            = 0;
	m_statistics.pagesInUse           = 0;
	m_statistics.mostRecentId         = 0;
	m_statistics.rolXoffStat          = 0;
	m_statistics.rolDownStat          = 0;
	m_statistics.bufferFull           = 0;
	m_statistics.fragsReceived        = 0;
	m_statistics.fragsReplaced        = 0;
	m_statistics.fragsTruncated       = 0;
	m_statistics.fragsCorrupted       = 0;
	m_statistics.level1RateHz         = 0;
	m_statistics.numberOfXoffs        = 0;
	m_statistics.numberOfLdowns 	  = 0;

	m_statistics.xoffPercentage 	  = 0;
	m_statistics.memreadcorruptions   = 0;
	m_statistics.fragsIndexed 		  = 0;
	m_statistics.fragsOutofsync 	  = 0;
	m_statistics.fragsShort 		  = 0;
	m_statistics.fragsLong			  = 0;
	m_statistics.fragsDiscarded 	  = 0;
	m_statistics.fragsFormatError 	  = 0;
	m_statistics.fragsMarkerError 	  = 0;
	m_statistics.fragsFramingError 	  = 0;
	m_statistics.fragsCtrlWordError   = 0;
	m_statistics.fragsDataWordError   = 0;
	m_statistics.fragsSizeMismatch 	  = 0;
	m_statistics.fragsRejected 		  = 0;
	m_statistics.fragsTxError 		  = 0;
	m_statistics.rolEnabled 		  = 0;

	m_statistics.fragsIncompleteOnDMA = 0;
	m_statistics.rolInputBandwidth 	  = 0;
	m_statistics.pagesRejected 		  = 0;
	m_statistics.pagesInvalid 		  = 0;
	m_statistics.pagesProvided 		  = 0;
	m_statistics.multipageIndexError  = 0;

	// Get the request mutex to protect the Fragment memory allocation
	m_requestMutex = DFFastMutex::Create((char *) "REQUESTMUTEX");
	if (configId == 0)
		m_infoflag = 1;
	else
		m_infoflag = 0;

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::constructor: m_memoryPool has " << m_memoryPool->numberOfPages() << " pages");
	m_pageIndex = new MemoryPageNP*[m_memoryPool->numberOfPages()];
	m_ticketPool = new u_int[m_memoryPool->numberOfPages()];
	for (int ticket = 0; ticket < m_memoryPool->numberOfPages(); ticket++)
		m_ticketPool[ticket] = ticket;
	m_nextTicket = 0;

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::constructor: Done");
}


/***********************************/
RobinNPDataChannel::~RobinNPDataChannel()
/***********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::destructor: Called");
	m_requestMutex->destroy();
	delete [] m_pageIndex;
	delete [] m_ticketPool;
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::destructor: Done");
}


/***********************************/
ISInfo* RobinNPDataChannel::getISInfo()
/***********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::getISInfo: Running");
	return &m_statistics;
}


/**************************************************/
void RobinNPDataChannel::setExtraParameters(int value)
/**************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::setExtraParameters: Called");
	m_l1id_modulo = value;
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::setExtraParameters: Done");
}


/*************************************************/
int RobinNPDataChannel::requestFragment(int level1Id)
/*************************************************/
{
	MemoryPageNP *mem_page;

	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::requestFragment (ROL " << m_id << "): Entering with L1ID = " << level1Id);

	// Lock the request mutex to allocate some memory for the requested fragment
	m_requestMutex->lock();
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::requestFragment: request Mutex locked");

	// Allocate memory for the fragment
	mem_page = m_memoryPool->getPage();

	u_int ticket = m_ticketPool[m_nextTicket++];

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::requestFragment: ticket = " << ticket);
	m_pageIndex[ticket] = mem_page;

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::requestFragment: Empty page created");
	m_requestMutex->unlock();
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::requestFragment: request Mutex unlocked");

	// Check if the allocated page is OK
	if (mem_page == 0)
	{
		DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPDataChannel::requestFragment(ROL " << m_id << "): Failed to get a page from the memory pool");
		CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_NOPAGE, "");
		throw(ex1);
	}

	// Get the memory page start address and reserve the max available contiguous space.
	PageAddress fragBufferAddress = mem_page->reserve(mem_page->capacity());


	// Call the Robin to get the fragment
	// TO BE DONE: shouldn't the data transfer be protected against possible transfers of data larger than the page size?
	/*if(level1Id == 1){
		std::cout << "L1 ID " << level1Id << " virt " << std::hex << (void *)fragBufferAddress.virt << " phys 0x" << fragBufferAddress.phys << std::dec << std::endl;
		std::cout << std::hex <<  "test1 " << 	*((unsigned int*)(fragBufferAddress.virt)) << std::endl;
		*((unsigned int*)(fragBufferAddress.virt)) = 0x99999999;
		std::cout << "test2 " << 	*((unsigned int*)(fragBufferAddress.virt)) << std::dec << std::endl;
	}*/


	m_rol.requestFragment(level1Id, fragBufferAddress.virt, fragBufferAddress.phys, ticket);

	//m_statistics.lastmsg = REQUESTFRAGMENT;
	//m_statistics.lastl1id = level1Id;

	//Remember the L1ID for getFragment
	mem_page->setUserdata(level1Id);

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::requestFragment: Returned ticket = " << ticket);
	return (ticket);
}

/*******************************************************/
EventFragment * RobinNPDataChannel::getFragment(int ticket)
/*******************************************************/
{
	u_int l1id1, l1id2;
	MemoryPageNP *mem_page;

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: called with ticket = " << ticket);

	mem_page = m_pageIndex[ticket];
	void *poll_address = mem_page->address();
	//std::cout << "ticket " << ticket << " poll address " << std::hex << poll_address << std::dec << std::endl;
	//Wait for the fragment
	m_rol.getFragment(poll_address,ticket);

	unsigned int fragmentSize = *(((unsigned int *)poll_address)+1) * sizeof(unsigned int);
	mem_page->release(mem_page->usedSize() - fragmentSize);
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: Parameters for mem_page->release:");
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: mem_page->usedSize() = " << mem_page->usedSize());
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: fragmentSize         = " << fragmentSize);

	//Seems to be the right time to create the robFragment
	//BG a mutex should not be needed here
	m_requestMutex->lock();
	ROBFragment *robFragment = new ROBFragment(reinterpret_cast<MemoryPage*>(mem_page),true); //HACK - FIX BY INTEGRATING CHANGES INTO ROSMEMORYPOOL

	//Now the page is locked by the ROBFragment: we can release it
	mem_page->free();

	// Return ticket to pool while mutex still locked
	m_ticketPool[--m_nextTicket] = ticket;

	//BG a mutex should not be needed here
	m_requestMutex->unlock();

	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: robFragment->size (in words) = 0x" << HEX(robFragment->size()));
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: fragment status = 0x" << HEX(robFragment->status()));
	//First of all let's have a look at some errors
	u_int statusword;
	u_int isok = robFragment->checkstatus(&statusword);
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment(ROL " << m_id << "): ROB status word = " << HEX(statusword) << " isok = " << isok);

	if (!isok)
	{
		DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPDataChannel::getFragment: fragment is too small");
		CREATE_ROS_EXCEPTION(ex15, ModulesException, PCIROBIN_ROBTOOSMALL, "RobinNPDataChannel: ROB fragment too small: The ROL ID is " << m_id);
		throw(ex15);
	}

	if ((robFragment->status() & 0xf0000000) == fragStatusLost)
	{
		m_statistics.numberOfNeverToCome++;
		DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPDataChannel::getFragment(ROL " << m_id << "): The fragment with L1ID = " << robFragment->level1Id() << " has been lost");

		u_int sid = robFragment->sourceIdentifier();
		u_int lid = robFragment->level1Id();
		CREATE_ROS_EXCEPTION(ex10, ModulesException, PCIROBIN_ROBSW, "RobinNPDataChannel: Lost fragment detected. The L1ID is 0x" << HEX(lid) << ". The ROB Source ID is 0x" << HEX(sid));
		ers::warning(ex10);
	}

	if ((robFragment->status() & 0xf0000000) == fragStatusPending)
	{
		DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::getFragment(ROL " << m_id << "): The requested fragment has not yet arrived");
		m_statistics.numberOfMayCome++;
	}

	l1id1 = mem_page->getUserdata();
	if (m_l1id_modulo)
	{
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: Setting L1ID to " << l1id1);
		robFragment->level1Id(l1id1);
	}
	else
	{
		//Check if the L1ID is the expected one
		l1id2 = robFragment->level1Id();
		DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: Comparing L1IDs " << l1id1 << " and " << l1id2);
		if (l1id1 != l1id2)
		{
			DEBUG_TEXT(DFDB_ROSFM, 5, "RobinNPDataChannel::getFragment(ROL " << m_id << "): Wrong L1ID " << l1id2 << " received but " << l1id1 << " expected");

			/*for(unsigned int counter = 0; counter < 40; ++ counter){
				std::cout << std::hex << ((unsigned int *)poll_address)[counter] << " ";
			}
			std::cout << std::dec << std::endl;*/
			//Set additional error bit in ROB status word
			u_int rob_status = robFragment->status();
			rob_status = rob_status | 0x2;
			robFragment->setStatus(rob_status);
			//m_statistics.statusErrors[1]++;   //Log the error for the second bit in the generic part of the status word


			CREATE_ROS_EXCEPTION(ex1, ModulesException, PCIROBIN_ILLL1ID, "ERROR in RobinNPDataChannel::getFragment(ROL " << m_id << "): Wrong L1ID 0x" << HEX(l1id2) << " received but 0x" << HEX(l1id1) << " expected.");
			ers::error(ex1);
			//throw(ex1);
		}
	}

	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::getFragment(ROL " << m_id << "): The L1ID is " << robFragment->level1Id());
	DEBUG_TEXT(DFDB_ROSFM, 20, "RobinNPDataChannel::getFragment: Done.");
	return(robFragment);
}

/**************************************************************************/
void RobinNPDataChannel::releaseFragment(const std::vector <u_int> *level1Ids)
/**************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(1): Called");

	std::vector<u_int>::const_iterator first = level1Ids->begin();
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::releaseFragment(1, ROL " << m_id << "): first L1ID = " << *first);

	m_rol.releaseFragment(level1Ids);

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(1): Done ");

}


/*****************************************************************************/
void RobinNPDataChannel::releaseFragmentAll(const std::vector <u_int> *level1Ids)
/*****************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragmentAll: Called");

	std::vector<u_int>::const_iterator first = level1Ids->begin();
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::releaseFragmentAll(ROL " << m_id << "): first L1ID = " << *first);

	m_rol.releaseFragmentAll(level1Ids);

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragmentAll: Done");
}


/**************************************************/
void RobinNPDataChannel::releaseFragment(int level1Id)
/**************************************************/
{
	std::vector<u_int> level1Ids;

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(2): Called ");

	level1Ids.push_back((u_int)level1Id);
	releaseFragment(&level1Ids);
	//m_statistics.lastmsg = RELEASEFRAGMENT;
	//m_statistics.lastl1id = level1Id;
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(2): Done ");

}


/**************************************************************************/
bool RobinNPDataChannel::releaseFragmentAck(const std::vector <u_int> *level1Ids)
/**************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(1): Called");

	std::vector<u_int>::const_iterator first = level1Ids->begin();
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::releaseFragment(1, ROL " << m_id << "): first L1ID = " << *first);

	bool result = m_rol.releaseFragment(level1Ids);
	//m_statistics.lastmsg = RELEASEFRAGMENT;
	//m_statistics.lastl1id = *first;

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(1): Done ");

	return result;
}


/*****************************************************************************/
bool RobinNPDataChannel::releaseFragmentAllAck(const std::vector <u_int> *level1Ids)
/*****************************************************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragmentAll: Called");

	std::vector<u_int>::const_iterator first = level1Ids->begin();
	DEBUG_TEXT(DFDB_ROSFM, 10, "RobinNPDataChannel::releaseFragmentAll(ROL " << m_id << "): first L1ID = " << *first);

	bool result = m_rol.releaseFragmentAll(level1Ids);
	//m_statistics.lastmsg = RELEASEFRAGMENT;
	//m_statistics.lastl1id = *first;

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragmentAll: Done");

	return result;
}


/**************************************************/
bool RobinNPDataChannel::releaseFragmentAck(int level1Id)
/**************************************************/
{
	std::vector<u_int> level1Ids;

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(2): Called ");

	level1Ids.push_back((u_int)level1Id);
	bool result = m_rol.releaseFragment(&level1Ids);
	//m_statistics.lastmsg = RELEASEFRAGMENT;
	//m_statistics.lastl1id = level1Id;
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::releaseFragment(2): Done ");

	return result;

}
/****************************************/
void RobinNPDataChannel::prepareForRun(void)
/****************************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::prepareForRun: Called");

	if (!m_l1id_modulo)
	{
		m_rol.clearStatistics();
		m_rol.setRolEnabled(true);
		m_rol.reset();
	}
	m_tsStopLast=std::chrono::system_clock::now();
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::prepareForRun: S-Link enabled");
}


/*********************************/
void RobinNPDataChannel::stopFE(void)
/*********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::stopFE: Called");
	m_rol.setRolEnabled(false);
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::stopFE: S-Link disabled");
}


/****************************/
void RobinNPDataChannel::probe()
/****************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::probe: Entered");

	RobinNPROLStats *statistics = m_rol.getStatistics();

	// This is just a selection of the most interesting parameters
	m_statistics.pagesFree     			= statistics->m_pagesFree;
	m_statistics.pagesInUse     		= statistics->m_pagesInUse;
	m_statistics.mostRecentId   		= statistics->m_mostRecentId;
	m_statistics.rolXoffStat    		= statistics->m_rolXoffStat;
	m_statistics.rolDownStat    		= statistics->m_rolDownStat;
	m_statistics.bufferFull     		= statistics->m_bufferFull;
	m_statistics.numberOfXoffs 			= statistics->m_xoffcount;
	m_statistics.numberOfNotDeleted 	= statistics->m_fragStat[fragsMissOnClear];
	m_statistics.fragsReceived 			= statistics->m_fragStat[fragsReceived];
	m_statistics.fragsTruncated			= statistics->m_fragStat[fragsTruncated];
	m_statistics.fragsCorrupted 		= statistics->m_fragStat[fragsCorrupted];
	m_statistics.fragsReplaced 			= statistics->m_fragStat[fragsReplaced];
	m_statistics.numberOfLdowns 		= statistics->m_ldowncount;

	m_statistics.xoffPercentage  		= statistics->m_xoffpercentage;
	m_statistics.memreadcorruptions 	= statistics->m_memreadcorruptions;
	m_statistics.fragsIndexed  			= statistics->m_fragStat[fragsAdded];
	m_statistics.fragsOutofsync 		= statistics->m_fragStat[fragsOutOfSync];
	m_statistics.fragsShort  			= statistics->m_fragStat[fragsShort];
	m_statistics.fragsLong 				= statistics->m_fragStat[fragsLong];
	m_statistics.fragsDiscarded  		= statistics->m_fragStat[fragsDiscarded];
	m_statistics.fragsFormatError  		= statistics->m_fragStat[fragsFormatError];
	m_statistics.fragsMarkerError  		= statistics->m_fragStat[fragsMarkerError];
	m_statistics.fragsFramingError  	= statistics->m_fragStat[fragsFramingError];
	m_statistics.fragsCtrlWordError 	= statistics->m_fragStat[fragsCtrlWordError];
	m_statistics.fragsDataWordError 	= statistics->m_fragStat[fragsDataWordError];
	m_statistics.fragsSizeMismatch  	= statistics->m_fragStat[fragsSizeMismatch];
	m_statistics.fragsRejected  		= statistics->m_fragStat[fragsRejected];
	m_statistics.fragsTxError  			= statistics->m_fragStat[fragsTxError];
	m_statistics.rolEnabled 			= m_rol.getRolState();

	m_statistics.fragsIncompleteOnDMA 	= statistics->m_fragsIncomplete;
	m_statistics.rolInputBandwidth 		= statistics->m_rolInputBandwidth;
	m_statistics.pagesRejected 			= statistics->m_pageStat[pagesSupressed];
	m_statistics.pagesInvalid 			= statistics->m_pageStat[pagesInvalid];
	m_statistics.pagesProvided 			= statistics->m_pageStat[pagesProvided];
	m_statistics.numberOfMayCome 		= statistics->m_fragStat[fragsPending];
	m_statistics.numberOfNeverToCome 	= statistics->m_fragStat[fragsNotAvail];
	//m_statistics.robId = m_channelToAtlas[m_rol.getRolId()];
	m_statistics.multipageIndexError 	= statistics->m_multipageIndexError;

	// level1 rate (for this channel)
	auto tsStop=std::chrono::system_clock::now();

	uint64_t numberOfLevel1Delta;
	if (m_numberOfLevel1Last == 0xffffffffffffffffLL)
		numberOfLevel1Delta = m_statistics.fragsReceived;
	else
		numberOfLevel1Delta = m_statistics.fragsReceived - m_numberOfLevel1Last;

	//m_statistics.level1RateHz = (u_int)(numberOfLevel1Delta / (float)ts_duration(m_tsStopLast, tsStop));
	auto elapsed=tsStop - m_tsStopLast;
	m_statistics.level1RateHz = (u_int)(numberOfLevel1Delta / ((float)std::chrono::duration_cast<std::chrono::microseconds> (elapsed).count()/1e6));

	m_tsStopLast = tsStop;

	m_numberOfLevel1Last = m_statistics.fragsReceived;
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::probe: Done");
}


/********************************/
void RobinNPDataChannel::clearInfo()
/********************************/
{
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::clearInfo: Entered");

	m_statistics.numberOfMayCome      = 0;
	m_statistics.numberOfNeverToCome  = 0;
	m_statistics.numberOfNotDeleted   = 0;
	m_statistics.pagesFree            = 0;
	m_statistics.pagesInUse           = 0;
	m_statistics.mostRecentId         = 0;
	m_statistics.rolXoffStat          = 0;
	m_statistics.rolDownStat          = 0;
	m_statistics.bufferFull           = 0;
	m_statistics.fragsReceived        = 0;
	m_statistics.fragsReplaced        = 0;
	m_statistics.fragsTruncated       = 0;
	m_statistics.fragsCorrupted       = 0;
	m_statistics.level1RateHz         = 0;
	m_statistics.numberOfXoffs        = 0;
	m_statistics.numberOfLdowns 	  = 0;

	m_statistics.xoffPercentage		  = 0;
	m_statistics.memreadcorruptions   = 0;
	m_statistics.fragsIndexed 		  = 0;
	m_statistics.fragsOutofsync 	  = 0;
	m_statistics.fragsShort 		  = 0;
	m_statistics.fragsLong 			  = 0;
	m_statistics.fragsDiscarded 	  = 0;
	m_statistics.fragsFormatError 	  = 0;
	m_statistics.fragsMarkerError 	  = 0;
	m_statistics.fragsFramingError 	  = 0;
	m_statistics.fragsCtrlWordError   = 0;
	m_statistics.fragsDataWordError   = 0;
	m_statistics.fragsSizeMismatch 	  = 0;
	m_statistics.fragsRejected 		  = 0;
	m_statistics.fragsTxError 		  = 0;
	m_statistics.rolEnabled 		  = 0;

	m_statistics.fragsIncompleteOnDMA = 0;
	m_statistics.rolInputBandwidth 	  = 0;
	m_statistics.pagesRejected 		  = 0;
	m_statistics.pagesInvalid 		  = 0;
	m_statistics.pagesProvided 		  = 0;
	m_statistics.multipageIndexError  = 0;

	//ts_clock(&m_tsStopLast);
	m_tsStopLast=std::chrono::system_clock::now();

	m_numberOfLevel1Last = 0xffffffffffffffffLL;

	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::clearInfo: Done");
}


/*****************************/
void RobinNPDataChannel::disable()
/*****************************/
{
	//DISABLE discard mode
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::enable: Called ");
	m_rol.configureDiscardMode(0);
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::enable: Done ");
}


/******************************/
void RobinNPDataChannel::enable()
/******************************/
{
	//ENABLE discard mode
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::disable: Called ");
	m_rol.configureDiscardMode(1);
	DEBUG_TEXT(DFDB_ROSFM, 15, "RobinNPDataChannel::disable: Done ");
}

void RobinNPDataChannel::restart(){
	m_rol.reset();
}
ECRStatisticsBlock RobinNPDataChannel::getECR(){
	return m_rol.getECR();
}

