/*********************************************/
/*  ATLAS ROS Software				         */
/*									         */
/*  Class: RobinNPDataChannel	             */
/*  Author: William Panduro Vazquez		RHUL */
/*         (j.panduro.vazquez@cern.ch)       */
/*********************************************/

#ifndef ROBINNPDATACHANNEL_H
#define ROBINNPDATACHANNEL_H

#include <vector>
#include <sstream>
#include "rcc_time_stamp/tstamp.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSEventFragment/EventFragment.h"
#include "DFSubSystemItem/Config.h"
#include "ROSModulesNP/ModulesException.h"
#include "ROSCore/DataChannel.h"
#include "ROSMemoryPoolNP/WrapperMemoryPoolNP.h"
#include "ROSMemoryPoolNP/MemoryPageNP.h"
#include "DFThreads/DFFastMutex.h"
#include "ROSInfo/RobinNPDataChannelInfo.h"
#include "ROSRobinNP/RolNP.h"

namespace ROS 
{
class RobinNPDataChannel : public DataChannel
{
public:
	//! Constructor.
	/*!
	 \param id system-wide readout link ID.
	 \param configId RobinNP internal readout link Id (0-11).
	 \param physicalAddress RobinNP internal readout link Id (0-11).
	 \param rol_ptr address of RolNP object to be interfaced with through the data channel.
	 \param RobinphysicalAddress PCIe slot number of RobinNP (from /proc/robinnp).
	 \param mpool pointer to DMA landing page memory pool.
	 */
	RobinNPDataChannel(u_int id, u_int configId, u_int physicalAddress, ROS::RolNP &rol_ptr, u_int RobinphysicalAddress, ROS::WrapperMemoryPoolNP *mpool);

	//! Destructor.
	~RobinNPDataChannel();
	//! Fragment receipt method.
	/*!
   	 \param ticket the request tracking ID produced by the requestFragment method.
   	 \return requested event fragment (or a dummy if data not yet available).
	 */
	RobinNPDataChannel(const RobinNPDataChannel&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNPDataChannel& operator=(const RobinNPDataChannel&) = delete; //!< Assignment Operator - NON-COPYABLE
	ROS::EventFragment *getFragment(int ticket);
	//! Fragment deletion method.
	/*!
       	 \param level1Ids vector of level1 IDs to delete for this readout link.
	 */
	void releaseFragment(const std::vector<u_int> * level1Ids);
	//! Fragment deletion method.
	/*!
       	 \param level1Ids vector of level1 IDs to delete for all readout links.
	 */
	void releaseFragmentAll(const std::vector<u_int> * level1Ids);
	//! Fragment deletion method.
	/*!
       	 \param level1Id individual level1 ID to delete for this readout link.
	 */
	void releaseFragment(int level1Id);
	//! Fragment deletion method.
	/*!
       	 \param level1Ids vector of level1 IDs to delete for this readout link.
       	  \result boolean completion flag (true = successful, false = failed).
	 */
	bool releaseFragmentAck(const std::vector<u_int> * level1Ids);
	//! Fragment deletion method.
	/*!
	       	 \param level1Ids vector of level1 IDs to delete for all readout links.
	       	 \result boolean completion flag (true = successful, false = failed).
	 */
	bool releaseFragmentAllAck(const std::vector<u_int> * level1Ids);
	//! Fragment deletion method.
	/*!
	       	 \param level1Id individual level1 ID to delete for this readout link.
	       	\result boolean completion flag (true = successful, false = failed).
	 */
	bool releaseFragmentAck(int level1Id);
	//! Fragment request method.
	/*!
	    \param level1Id the ID of the event being requested.
	    \return tracking ID associated with request.
	 */
	int requestFragment(int level1Id);
	//! Extra paremeters control method.
	/*!
    \param value flag to toggle extra parameters (mainly used for forced L1ID).
	 */
	void setExtraParameters(int value);
	//! IS info return method.
	/*!
    \return pointer to IS information block.
	 */
	virtual ISInfo*  getISInfo();
	//! Clear IS information.
	void clearInfo();
	//! Disable readout link.
	void stopFE();
	//! Activate readout link.
	void prepareForRun();
	//! IS info collection method.
	void probe();
	//! Begin discard mode.
	void disable();
	//! End discard mode.
	void enable();
	//! Get Event Counter Return Statistics.
	ECRStatisticsBlock getECR();
	//! Reset a readout link.
	void restart();

private:
	ROS::WrapperMemoryPoolNP *m_memoryPool;
	ROS::RolNP & m_rol;
	DFCountedPointer<Config> m_configuration;
	DFFastMutex *m_requestMutex;            // The request mutex necessary to access the memory pool
	unsigned long long int m_gcdeleted;
	unsigned long long int m_gcfree;
	u_int m_id;
	u_int m_l1id_modulo;
	u_int m_RobinphysicalAddress;
	u_int m_infoflag;
	std::chrono::time_point<std::chrono::system_clock> m_tsStopLast;
	//tstamp m_tsStopLast;         // time stamp for level1 rate calculation
	u_int64_t m_numberOfLevel1Last;
	RobinNPDataChannelInfo m_statistics;
	u_int m_nextTicket;
	u_int *m_ticketPool;
	MemoryPageNP** m_pageIndex;

	enum RobinNP_Messages
	{
		NOMESSAGE = 0,
		REQUESTFRAGMENT,
		RELEASEFRAGMENT,
		COLLECTGARBAGE
	};
};
}
#endif
