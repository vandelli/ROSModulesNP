#ifndef ROBINNPDESCRIPTORREADOUTMODULE_H
#define ROBINNPDESCRIPTORREADOUTMODULE_H

#include <vector>
#include <string>
#include <map>
#include <chrono>

#include "tbb/concurrent_queue.h"

#include "ROSDescriptorNP/NPReadoutModule.h"
#include "ROSDescriptorNP/NPRequestDescriptor.h"



#include "ROSRobinNP/RobinNP.h"
#include "ROSRobinNP/RolNP.h"

#include "ROSCore/ReadoutModule.h"

#include "DFSubSystemItem/Config.h"
#include "DFThreads/DFCountedPointer.h"
#include "ROSInfo/RobinNPDescriptorReadoutModuleInfo.h"

#include "DFdal/RobinNPDescriptorReadoutModule.h"
#include "DFdal/RobinConfiguration.h"
#include "DFdal/RobinExpertParameter.h"
#include "DFdal/RobinChannelConfiguration.h"
#include "DFdal/ROS.h"
//#include "DFdal/InputChannel.h"
#include "DFdal/ReadoutConfiguration.h"
#include "DFdal/DFParameters.h"
#include "dal/ResourceSet.h"
#include "dal/ResourceBase.h"
#include "DFdal/RobinDataChannel.h"
#include "DFdal/MemoryPool.h"
#include "dal/Detector.h"
namespace ROS 
{
class WrapperMemoryPoolNP;

class RobinNPDescriptorReadoutModule : public NPReadoutModule
{
public:
	//! Constructor.
	RobinNPDescriptorReadoutModule(void);
	//! Destructor.
	virtual ~RobinNPDescriptorReadoutModule() noexcept;
	//! Configuration storage method.
	/*!
				\param configuration configuration block for storage and later use.
	 */
	RobinNPDescriptorReadoutModule(const RobinNPDescriptorReadoutModule&) = delete; //!< Copy-Constructor - NON-COPYABLE.
	RobinNPDescriptorReadoutModule& operator=(const RobinNPDescriptorReadoutModule&) = delete; //!< Assignment Operator - NON-COPYABLE
	virtual void setup(DFCountedPointer<Config> configuration);
	//! Data channel access method.
	/*!
				\return vector of data channels associated with the readout module.
	 */
	virtual const std::vector<DataChannel *> * channels();
	//! Enables all readout links and starts RobinNP control threads
	/*!
				\param cmd run control command reference
	 */
	virtual void prepareForRun(const daq::rc::TransitionCmd& cmd);
	//! Disables all readout links and stops RobinNP control threads
	/*!
				\param cmd run control command reference
	 */
	virtual void stopGathering(const daq::rc::TransitionCmd& cmd);
	//! Instantiate and configure RobinNP object
	/*!
				\param cmd run control command reference
				\throw PCIROBIN_PSIZE Inconsistent page size for all channels
	 */
	virtual void configure(const daq::rc::TransitionCmd& cmd);
	//! De-configure and delete RobinNP object
	/*!
				\param cmd run control command reference
	 */
	virtual void unconfigure(const daq::rc::TransitionCmd& cmd);
	//! Clear all data driven trigger statistics
	virtual void clearInfo();
	//! Publish data driven trigger statistics
	virtual void publishFullStatistics(void);
	//! Place data channels in discard mode
	/*!
				\param uid vector of data channel names
	 */
	virtual void disable(const std::vector<std::string>& uid);
	//! Take data channels out of discard mode
	/*!
				\param uid vector of data channel names
	 */
	virtual void enable(const std::vector<std::string>& uid);
	//! Fragment request method
	/*!
				\param descriptor ROS descriptor containing fragment request details
	 */
	void requestFragment(NPRequestDescriptor *descriptor);

	//! Fragment deletion method
	/*!
				\param l1IDs vector of level1 IDs to delete for all readout links associated with the readout module
	 */
	void clearFragments(std::vector<unsigned int> &l1IDs);
	//! Descriptor channel setup method
	/*!
				\param descriptor ROS descriptor to be associated with readout links
	 */
	void registerChannels(uint32_t &localBaseID, std::vector<tbb::concurrent_bounded_queue<DataPage*>*>& queues, std::vector<std::vector<uint32_t> >& atlasIDs);
	//! Data collector queue registration method
	/*!
				\param descriptorQueue pointer to descriptor queue for outgoing completed requests
	 */
	virtual void setCollectorQueue(
	         tbb::concurrent_bounded_queue<NPRequestDescriptor*>* descriptorQueue);

	//! IS info collection method
	/*!
				\return pointer to IS information block
	 */
	ISInfo* getISInfo();

	virtual void getL1CountPointers(std::vector<unsigned int *>& statsVec, std::vector<bool*>& enableVec);

	virtual void user(const daq::rc::UserCmd& command);

private:

	void dumpDalConfig();
	void initStatistics();

	RobinNP* m_robIn;
	u_int m_physicalAddress;           // The RobIn ID number (= PCI logical number)
	u_int m_numberOfDataChannels;
	u_int m_firstRolPhysicalAddress;
	u_int *m_rolPhysicalAddressTable;
	//std::map <std::string, RobinNPDescriptorChannel *> m_uidMap;
	//std::vector <DataChannel *> m_dataChannels;
	static bool s_firstModuleFound;
	//std::map<unsigned int, std::pair<unsigned int, unsigned int>> m_channelMap;
	RobinNPDescriptorReadoutModuleInfo m_statistics;
	uint64_t *m_numberOfLevel1Last;
	//tstamp m_tsStopLast;
	std::chrono::time_point<std::chrono::system_clock> m_tsStopLast;
	unsigned int m_firstChannelIndex;
	std::vector<unsigned int> m_channelVector;
	std::vector<unsigned int> m_channelToPhysical;
	std::map <std::string, unsigned int> m_uidMap;

	const daq::df::RobinNPDescriptorReadoutModule* m_configurationDal;
	const daq::core::ResourceSet* m_channelsDal;
	std::vector<const daq::core::ResourceBase*>* m_channelListDal;
	const daq::df::RobinConfiguration* m_robinNPConfigurationDal;


};
/*! \class RobinNPDescriptorModule
 *  \brief RobinNP descriptor based readout module class.
 *
 *  This class provides the ddescriptor based readout module interface to all RobinNP functionality.
 *
 */
}

#endif 
